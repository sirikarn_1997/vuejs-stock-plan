import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/About.vue"),
    },
    {
        path: "/createplan",
        name: "createplan",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/createplan.vue"),
    },
    {
        path: "/closeplan",
        name: "Closeplan",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/Closeplan.vue"),
    },
    {
        path: "/upload",
        name: "Upload",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/Upload.vue"),
    }, {
        path: "/editplan/:doc",
        name: "Editplan",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/Editplan.vue"),
    }, {
        path: "/report",
        name: "Report",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/Report.vue"),
    }, {
        path: "/EditNumStock",
        name: "EditNumStock",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/EditNumStock.vue"),
    },
    {
        path: "/LineBOT",
        name: "LineBOT",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/LineBOT.vue"),
    },

];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;